import MySQLdb
import config
import utils
import pika
import pdb

MYSQL_ATTEMTPS = 5
RABBITMQ_ATTEMTPS = 5

cfg = config.config

def checkup_mysql():
  i = 0
  while True:
    if i == MYSQL_ATTEMTPS:
      return False
    else:
      try:
        mysql_conn = MySQLdb.connect(user=cfg["mysql_server"]["login"], passwd=cfg["mysql_server"]["password"], db=cfg["mysql_server"]["db_name"], connect_timeout=???)
        mysql_conn.ping(True)
        cur = db_conn.cursor()
        cur.execute("SELECT 1 = 1")
        db_conn.commit()
        cur.close()
        return True
      except MySQLdb.OperationalError as e:
        i += 1

def checkup_rabbitmq():
  i = 0
  while True:
    if i == RABBITMQ_ATTEMTPS:
      return False
    try:
      cnn = pika.BlockingConnection(pika.ConnectionParameters(cfg["rabbitmq_server"]["host"]))
      chnl = cnn.channel()
      chnl.queue_declare(queue="test", auto_delete=True)
      return True
    except pika.exceptions.ConnectionClosed:
      i += 1

if __name__ == "__main__":
  res1 = utils.wait_for(checkup_rabbitmq)
  res2 = utils.wait_for(checkup_mysql)
  subj = ""
  body = ""
  if res1 == False or res2 == False:
    subj = utils.EMAIL_SUBJECT_FAILURE
    if res1 == False:
      body += "RabbitMQ -> Failure"
    if res2 == False:
      body += "MySql -> Failure"
  else:
    subj = utils.EMAIL_SUBJECT_OK
    body = """
      RabbitMQ -> Ok
      MySql -> Ok
    """

  utils.send_email(subj, body)