#!/bin/bash

result=$(ps --no-headers -C "python3 elexon_manager.py" -o args,state)
if [ "$result" = "" ]
then
  echo "Empty"
  # todo send mail
fi

result=$(ps --no-headers -C "python3 rabbitmq_manager.py" -o args,state)
if [ "$result" = "" ]
then
  echo "Empty"
  # todo send mail
fi