import os
import yaml

config = None
cur_dir = os.path.dirname(os.path.abspath(__file__))
with open(os.path.join(cur_dir, "config.yml"), "r") as f:
  config = yaml.load(f.read())