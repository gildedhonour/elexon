create database IF NOT EXISTS toh_power_markets;
USE toh_power_markets;

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_bod` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `publication_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `bid_offer_pair_number` VARCHAR(100) DEFAULT NULL,
  `from_time` TIMESTAMP NULL DEFAULT NULL,
  `to_time` TIMESTAMP NULL DEFAULT NULL,
  `from_level` INT DEFAULT NULL,
  `to_level` INT DEFAULT NULL,
  `bid_price` DECIMAL(10, 5) DEFAULT NULL,
  `offer_price` DECIMAL(10, 5) DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_fpn` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `bm_unit_type` VARCHAR(1) DEFAULT NULL,
  `lead_party_name` VARCHAR(100) DEFAULT NULL, 
  `ngc_bm_unit_name` VARCHAR(100) DEFAULT NULL, 
  `from_time` TIMESTAMP NULL DEFAULT NULL,
  `to_time` TIMESTAMP NULL DEFAULT NULL,
  `from_level` INT DEFAULT NULL,
  `to_level` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_mel` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `bm_unit_type` VARCHAR(1) DEFAULT NULL,
  `lead_party_name` VARCHAR(100) DEFAULT NULL, 
  `ngc_bm_unit_name` VARCHAR(100) DEFAULT NULL, 
  `from_time` TIMESTAMP NULL DEFAULT NULL,
  `to_time` TIMESTAMP NULL DEFAULT NULL,
  `from_level` INT DEFAULT NULL,
  `to_level` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_mil` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `bm_unit_type` VARCHAR(1) DEFAULT NULL,
  `lead_party_name` VARCHAR(100) DEFAULT NULL, 
  `ngc_bm_unit_name` VARCHAR(100) DEFAULT NULL, 
  `from_time` TIMESTAMP NULL DEFAULT NULL,
  `to_time` TIMESTAMP NULL DEFAULT NULL,
  `from_level` INT DEFAULT NULL,
  `to_level` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_sel` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `effective_from` TIMESTAMP NULL DEFAULT NULL,
  `mw` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_sil` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `effective_from` TIMESTAMP NULL DEFAULT NULL,
  `mw` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_ndz` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `effective_from` TIMESTAMP NULL DEFAULT NULL,
  `minutes` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_mzt` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `effective_from` TIMESTAMP NULL DEFAULT NULL,
  `minutes` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_mnzt` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `effective_from` TIMESTAMP NULL DEFAULT NULL,
  `minutes` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_rure` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `effective_from` TIMESTAMP NULL DEFAULT NULL,
  `rate_1` INT DEFAULT NULL,
  `elbow_2` INT DEFAULT NULL,
  `rate_2` INT DEFAULT NULL,
  `elbow_3` INT DEFAULT NULL,
  `rate_3` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_rdre` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `effective_from` TIMESTAMP NULL DEFAULT NULL,
  `rate_1` INT DEFAULT NULL,
  `elbow_2` INT DEFAULT NULL,
  `rate_2` INT DEFAULT NULL,
  `elbow_3` INT DEFAULT NULL,
  `rate_3` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_ruri` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `effective_from` TIMESTAMP NULL DEFAULT NULL,
  `rate_1` INT DEFAULT NULL,
  `elbow_2` INT DEFAULT NULL,
  `rate_2` INT DEFAULT NULL,
  `elbow_3` INT DEFAULT NULL,
  `rate_3` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_disbsad` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `action_id` INT DEFAULT NULL,
  `action_cost` INT DEFAULT NULL,
  `action_volume` INT DEFAULT NULL,
  `so_flag` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_lolp` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `lolp8` VARCHAR(100) DEFAULT NULL,
  `drm8` VARCHAR(100) DEFAULT NULL,
  `lolp4` VARCHAR(100) DEFAULT NULL,
  `drm4` VARCHAR(100) DEFAULT NULL,
  `lolp2` VARCHAR(100) DEFAULT NULL,
  `drm2` VARCHAR(100) DEFAULT NULL,
  `lolp1` VARCHAR(100) DEFAULT NULL,
  `drm1` VARCHAR(100) DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_non_bm_stor_volume` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `publication_date’` TIMESTAMP NULL DEFAULT NULL,
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `nonbm_publish_time` TIMESTAMP NULL DEFAULT NULL,
  `nonbm_mwh` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_netbsad` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `esca` INT DEFAULT NULL,
  `esva` INT DEFAULT NULL,
  `ssva` INT DEFAULT NULL,
  `spa` INT DEFAULT NULL,
  `ebca` INT DEFAULT NULL,
  `ebva` INT DEFAULT NULL,
  `sbva` INT DEFAULT NULL,
  `bpa` INT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_boalf` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `publication_date` TIMESTAMP NULL DEFAULT NULL,
  
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `bm_unit_type` VARCHAR(1) DEFAULT NULL,
  `lead_party_name` VARCHAR(100) DEFAULT NULL, 
  `ngc_bm_unit_name` VARCHAR(100) DEFAULT NULL, 
  `acceptance_id` INT DEFAULT NULL,
  `acceptance_time` TIMESTAMP NULL DEFAULT NULL,
  `deemed_flag` VARCHAR(1) DEFAULT NULL,
  `so_flag` VARCHAR(3) DEFAULT NULL,
  `stor_flag` VARCHAR(1) DEFAULT NULL,
  `from_time` TIMESTAMP NULL DEFAULT NULL,
  `from_level` INT DEFAULT NULL,
);

CREATE TABLE IF NOT EXISTS `toh_power_markets`.`uk_detailed_system_prices` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `added_on` TIMESTAMP DEFAULT now(),
  `settlement_date` TIMESTAMP NULL DEFAULT NULL,
  `settlement_period` INT DEFAULT NULL,
  `type` VARCHAR(10) DEFAULT NULL,
  `index` INT DEFAULT NULL,
  `bm_unit_id` VARCHAR(100) DEFAULT NULL, 
  `acceptance_id` INT DEFAULT NULL,
  `bop_id` INT DEFAULT NULL,
  `cadl_flag` VARCHAR(1) DEFAULT NULL,
  `so_flag` VARCHAR(1) DEFAULT NULL,
  `re_priced` VARCHAR(1) DEFAULT NULL,
  `rsp` VARCHAR(100) DEFAULT NULL,
  `price` VARCHAR(100) DEFAULT NULL, 
  `volumw` VARCHAR(100) DEFAULT NULL, 
  `dmat_adj_vol` VARCHAR(100) DEFAULT NULL, 
  `niv_adj_vol` VARCHAR(100) DEFAULT NULL, 
  `par_adj_vol` VARCHAR(100) DEFAULT NULL, 
  `final_price` VARCHAR(100) DEFAULT NULL, 
  `tlm` VARCHAR(100) DEFAULT NULL, 
  `qapo_x_tlm` VARCHAR(100) DEFAULT NULL, 
  `qapo_x_po_x_tlm` VARCHAR(100) DEFAULT NULL 
);