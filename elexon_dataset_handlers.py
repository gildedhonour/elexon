import pdb
import xml.etree.ElementTree as ET
import json
import logging
import pymysql
import utils
import config
import time

cfg = config.config
mysql_conn = None

log_file_name = "logs/elexon_dataset_handlers/{}_{}_{}.log".format(time.strftime("%d"), time.strftime("%b").lower(), time.strftime("%Y"))
fh = logging.FileHandler(log_file_name)
logger = logging.getLogger(__name__)
logger.addHandler(fh)

stderr_logger = logging.StreamHandler()
stderr_logger.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
logger.addHandler(stderr_logger)

def get_mysql_connection():
  global mysql_conn
  if mysql_conn is None:
   mysql_conn = pymysql.connect(user=cfg["mysql_server"]["login"], password=cfg["mysql_server"]["password"], db=cfg["mysql_server"]["db_name"])

  return mysql_conn

def reconnect_to_mysql():
  global mysql_conn
  mysql_conn = None
  get_mysql_connection()

class Dataset(object):
  remote_dataset_id = None
  local_dataset_id = None
  rabbitmq_channel = None

  def get_pubts(root):
    for x in root:
      if x.tag == "pubTs":
        return x.text



  # def insert_into_mysql(body):
  #   db_conn = get_mysql_connection()
  #   cur = db_conn.cursor()
  #   raw_data = json.loads(body.decode("utf-8"))
  #   cur.execute(self.mysql_insert_str, self.get_mysql_insert_str_data(raw_data))
  #   db_conn.commit()
  #   cur.close()

# BOD Data
class BodData(Dataset):
  remote_dataset_id = "BOD"
  local_dataset_id = "uk_bod"

  # todo - remove?
  fields_mapped = {
    "SD": "settlement_date",
    "SP": "settlement_period",
    "subject": "bm_unit_id",
    "NN": "bid_offer_pair_number",
    "TS1": "from_time",
    "VB1": "from_level",
    "TS2": "to_time",
    "VB2": "to_level",
    "BP": "bid_price",
    "OP": "offer_price"
  }

  # todo parse publication_date (pubTs)
  mysql_insert_str = """INSERT INTO {tbl_name}(
        publication_date, 
        settlement_date, 
        settlement_period, 
        
        bm_unit_id, 
        bid_offer_pair_number, 
        from_time, 
        
        to_time, 
        from_level, 
        to_level, 
        
        bid_price, 
        offer_price
      ) VALUES(
          %s, %s, %s,    
          %s, %s, %s, 
          %s, %s, %s, 
          %s, %s)
    """.format(tbl_name=local_dataset_id)

  def get_mysql_insert_str_data(raw_data):
    return (raw_data["publication_date"], raw_data["settlement_date"], raw_data["settlement_period"], raw_data["bm_unit_id"], 
      raw_data["bid_offer_pair_number"], raw_data["from_time"], raw_data["to_time"], raw_data["from_level"], 
      raw_data["to_level"], raw_data["bid_price"], raw_data["offer_price"],)

  def parse_elexon_message(msg):
    ret_data_array = []
    ret_data_item = {}
    root = ET.fromstring(msg)
    
    
    pdb.set_trace()


    pub_date = Dataset.get_pubts(root)

    all_msg = root.findall("msg")
    for msg_item in all_msg:
      for msg_item_item in msg_item:
        title = msg_item_item.tag
        value = msg_item_item.text

        # 2 rows
        if msg_item_item.tag == "row":
          for rows_item, i in zip(msg_item.findall("row"), [1, 2]):
            for row_item in rows_item:
              row_item_title = row_item.tag + str(i)
              if row_item_title in self.fields_mapped:
                ret_data_item[self.fields_mapped[row_item_title]] = row_item.text
        
        # standard fields
        else:
          if title in self.fields_mapped:
            ret_data_item[self.fields_mapped[title]] = value
      
      for k, v in ret_data_item.items():
        if ":GMT" in v:
          ret_data_item[k] = v[:-4]

      # pdb.set_trace()
      ret_data_item["publication_date"] = pub_date
      ret_data_array.append(json.dumps(ret_data_item))

    return ret_data_array
  
  def on_from_rabbitmq_message(ch, method, properties, body):
    # insert_into_mysql(body)
    db_conn = get_mysql_connection()
    cur = db_conn.cursor()
    raw_data = json.loads(json.loads(body.decode("utf-8"))) #todo
    cur.execute(self.mysql_insert_str, self.get_mysql_insert_str_data(raw_data))
    db_conn.commit()
    cur.close()


# class NonBmStorInstructedVolumeData(Dataset):
class NonbmData(Dataset):
  remote_dataset_id = "NONBM"
  data_store_name = "uk_non_bm_stor_volume"
  fields_mapped = {
    "msg": {
      "TP": "nonbm_publish_time"
    },
    "row": {
      "SD": "settlement_date",
      "SP": "settlement_period",
      "NB": "nonbm_mwh"
    }
  }

  def parse_elexon_message(msg):
    ret_data_array = []
    ret_data_item = {}
    root = ET.fromstring(msg)
    pub_date = Dataset.get_pubts(root)
    all_msg = root.findall("msg")

    for msg_item in all_msg:
      for msg_item_item in msg_item:
        # row
        if msg_item_item.tag == "row":
          for rows_item in msg_item.findall("row"):
            for row_item in rows_item:
              if row_item.tag in NonbmData.fields_mapped["row"]: # todo self.fields_mapped
                ret_data_item[NonbmData.fields_mapped["row"][row_item.tag]] = row_item.text
        
        # msg
        elif msg_item_item.tag == "msg":
          title = msg_item_item.tag
          value = msg_item_item.text

          if title in NonbmData.fields_mapped["msg"]:
            ret_data_item[NonbmData.fields_mapped["msg"][title]] = value
      
      for k, v in ret_data_item.items():
        if ":GMT" in v:
          ret_data_item[k] = v[:-4]

      # pdb.set_trace()
      ret_data_item["publication_date"] = pub_date
      ret_data_array.append(json.dumps(ret_data_item))

    return ret_data_array

  def on_from_rabbitmq_message(ch, method, properties, body):
    # insert_into_mysql(body)
    db_conn = get_mysql_connection()
    cur = db_conn.cursor()
    raw_data = json.loads(json.loads(body.decode("utf-8"))) #todo
    cur.execute(self.mysql_insert_str, self.get_mysql_insert_str_data(raw_data))
    db_conn.commit()
    cur.close()
  

class BoalfData(Dataset):
  remote_dataset_id = "PHYBMDATA"
  data_store_name = "uk_boalf"

  fields_mapped = {
    "msg": {
      "subject": "bm_unit_id",
      "NK": "acceptance_id",
      "TA": "acceptance_time",
      "SO": "so_flag",
      "PF": "stor_flag",
      "AD": "deemed_flag"
    }, 
    "row": {
      "TS": "from_time",
      "VA": "from_level"
    }
  }

  def parse_elexon_message(msg):
    ret_data_array = []
    ret_data_item = {}
    root = ET.fromstring(msg)
    
    
    # pdb.set_trace()


    pub_date = Dataset.get_pubts(root)

    all_msg = root.findall("msg")
    for msg_item in all_msg:
      for msg_item_item in msg_item:
        title = msg_item_item.tag
        value = msg_item_item.text

        # 2 rows
        if msg_item_item.tag == "row":
          for rows_item, i in zip(msg_item.findall("row"), [1, 2]):
            for row_item in rows_item:
              row_item_title = row_item.tag + str(i)
              if row_item_title in self.fields_mapped:
                ret_data_item[self.fields_mapped[row_item_title]] = row_item.text
        
        # standard fields
        else:
          if title in self.fields_mapped:
            ret_data_item[self.fields_mapped[title]] = value
      
      for k, v in ret_data_item.items():
        if ":GMT" in v:
          ret_data_item[k] = v[:-4]

      # pdb.set_trace()
      ret_data_item["publication_date"] = pub_date
      ret_data_array.append(json.dumps(ret_data_item))

    return ret_data_array
  
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class NetbsadData(Dataset):
  remote_dataset_id = "NETBSAD"
  data_store_name = "uk_netbsad"
  
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass








class FpnData(Dataset):
  remote_dataset_id = "PHYBMDATA"
  local_dataset_id = "uk_fpn"
  mysql_insert_str = ""

  def get_mysql_insert_str_data(raw_data):
    pass

  def parse_elexon_message(msg):
    pass
  
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class MelData(Dataset):
  remote_dataset_id = "PHYBMDATA"
  data_store_name = "uk_mel"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass  

class MilData(Dataset):
  remote_dataset_id = "PHYBMDATA"
  data_store_name = "uk_mil"
  
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass  

class SelData(Dataset):
  remote_dataset_id = "DYNBMDATA"
  data_store_name = "uk_sel"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class SilData(Dataset):
  remote_dataset_id = "DYNBMDATA"
  data_store_name = "uk_sil"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class NdzData(Dataset):
  remote_dataset_id = "DYNBMDATA"
  data_store_name = "uk_ndz"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class MztData(Dataset):
  remote_dataset_id = "DYNBMDATA"
  data_store_name = "uk_mzt"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class MnztData(Dataset):
  remote_dataset_id = "DYNBMDATA"
  data_store_name = "uk_mnzt"
  
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class RureData(Dataset):
  remote_dataset_id = "DYNBMDATA"
  data_store_name = "uk_rure"
  
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class RdreData(Dataset):
  remote_dataset_id = "DYNBMDATA"
  data_store_name = "uk_rdre"

  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class RuriData(Dataset):
  remote_dataset_id = "DYNBMDATA"
  data_store_name = "uk_ruri"
  
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class RureData(Dataset):
  remote_dataset_id = "DYNBMDATA"
  data_store_name = "uk_rure"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class DisbsadData(Dataset):
  remote_dataset_id = "DISBSAD"
  data_store_name = "uk_disbsad"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class LolpData(Dataset):
  remote_dataset_id = "LOLPDRM"
  data_store_name = "uk_lolp"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class IndicativeSystemPriceStackData(Dataset):
  remote_dataset_id = "DETSYSPRICES"
  data_store_name = "uk_detailed_system_prices"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass

class DetsyspricesData(Dataset):
  remote_dataset_id = "BOD"
  local_dataset_id = "uk_bod"
  def on_from_rabbitmq_message(ch, method, properties, body):
    print("FpnData on_from_rabbitmq_message")
    pass