import stomp
import time
import socket
import pika
import xml
import xml.etree.ElementTree as ET
import json
import logging
import pdb
import utils
import elexon_dataset_handlers
import rabbitmq_manager
import config
import threading
import os

cfg = config.config

log_file_name = "logs/elexon_manager/{}_{}_{}.log".format(time.strftime("%d"), time.strftime("%b").lower(), time.strftime("%Y"))
logger = logging.getLogger(__name__)
# fh = logging.FileHandler(log_file_name)
# logger.addHandler(fh)

stderr_logger = logging.StreamHandler()
stderr_logger.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
logger.addHandler(stderr_logger)

is_connected = False
subscription_types = cfg["subscription_types"]
destination_name = "/topic/{}".format(cfg["var"]["topic_name"])
selector = None
if len(subscription_types) > 0:
  selector = " OR ".join(["JMSType = '{}'".format(jtp_item.upper()) for jtp_item in subscription_types])

class ElexonListener(stomp.ConnectionListener):
  def on_message(self, headers, message):
    hdr = headers["type"].lower()
    logging.info("on_message from Elexon: " + hdr)

    if hdr in subscription_types:
      # todo - we don't know how to parse netbsad yet
      if hdr != "nonbm" or hdr != "boalf" or hdr != "bod" :
        return

      cls_handler = vars(elexon_dataset_handlers)[utils.subscription_types_objects[hdr]]

      # pdb.set_trace()

      msgs = cls_handler.parse_elexon_message(message)
      for x in msgs:
        utils.send_to_rabbitmq(hdr, x)

  def on_error(self, headers, message):
    logging.error("on_error when receiving a message from Elexon:\r\n{}".format(message))

  def on_disconnected(self):
    logging.info("on_disconnected from Elexon")

    global is_connected
    is_connected = False
    connect_to_elexon()
    utils.loop()

  def on_connected(self, headers, body):
    # logging.info("on_connected to Elexon")
    print("on_connected to Elexon")

def connect_to_elexon():
  global is_connected
  while not is_connected:
    try:
      logging.info("Connecting...")

      # pdb.set_trace()

      conn = stomp.Connection(host_and_ports=[(cfg["elexon_server"]["url"], cfg["elexon_server"]["port"])], use_ssl=True)
      conn.set_listener("ElexonListener", ElexonListener())
      conn.start()

      # pdb.set_trace()
      
      conn.connect(cfg["elexon_server"]["api_key"], cfg["elexon_server"]["api_key"], wait=True, headers={"client-id": cfg["var"]["client_id"]})
      hdrs = {"activemq.subscriptionName": cfg["var"]["subscription_name"]}
      if selector != None:
        hdrs["selector"] = selector

      conn.subscribe(destination=destination_name, id=0, ack="auto", headers=hdrs)
      is_connected = True
    except socket.error:
      logging.error("on_error when receiving a message from Elexon:\r\n{}".format(message))
      pass

# todo - refactor
def get_heart_beat_file_name():
  # todo - from config
  dirr = "logs/elexon_manager/heart_beat/{d}_{mon}_{y}/{h}".format(d=time.strftime("%d"), mon=time.strftime("%b").lower(), 
    y=time.strftime("%Y"), h=time.strftime("%H"))
  
  if not os.path.exists(dirr):
    os.makedirs(dirr)

  return os.path.join(dirr, "{}.txt".format(time.strftime("%M")))

def heart_beat():
  threading.Timer(60.0, heart_beat).start()
  file_name = get_heart_beat_file_name()
  with open(file_name, "w") as f:
    f.write("ok")

if __name__ == "__main__":
  heart_beat()
  rabbitmq_manager.get_rabbitmq_connection()
  connect_to_elexon()
  utils.loop()