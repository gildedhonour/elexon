import pika
import stomp
import pdb #todo
import socket
import pika

# import MySQLdb
import pymysql

import json
import logging
import utils
import elexon_dataset_handlers
import config
import time
import threading
import os

cfg = config.config
rbt_conn = None

log_file_name = "logs/rabbitmq_manager/{}_{}_{}.log".format(time.strftime("%d"), time.strftime("%b").lower(), time.strftime("%Y"))
fh = logging.FileHandler(log_file_name)
logger = logging.getLogger(__name__)
logger.addHandler(fh)

stderr_logger=logging.StreamHandler()
stderr_logger.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
logger.addHandler(stderr_logger)

def get_rabbitmq_connection():
  global rbt_conn
  if rbt_conn is None:
    rbt_conn = pika.BlockingConnection(pika.ConnectionParameters(cfg["rabbitmq_server"]["host"]))

  return rbt_conn

def reconnect_to_rabbitmq():
  global rbt_conn
  rbt_conn = None
  get_rabbitmq_connection()

# todo - refactor
def get_heart_beat_file_name():
  # todo - from config
  dirr = "logs/rabbitmq_manager/heart_beat/{d}_{mon}_{y}/{h}".format(d=time.strftime("%d"), mon=time.strftime("%b").lower(), 
    y=time.strftime("%Y"), h=time.strftime("%H"))
  
  if not os.path.exists(dirr):
    os.makedirs(dirr)

  return os.path.join(dirr, "{}.txt".format(time.strftime("%M")))

def heart_beat():
  threading.Timer(60.0, heart_beat).start()
  file_name = get_heart_beat_file_name()
  with open(file_name, "w") as f:
    f.write("ok")

if __name__ == "__main__":
  heart_beat()
  chnl = get_rabbitmq_connection().channel()
  for x in utils.subscription_types_objects:
    # pdb.set_trace()

    chnl.queue_declare(queue=x, durable=True)
    cls = vars(elexon_dataset_handlers)[utils.subscription_types_objects[x]]
    chnl.basic_consume(cls.on_from_rabbitmq_message, queue=x, no_ack=True)
    logging.info("start_consuming for: {}".format(x))


  # pdb.set_trace()

  chnl.start_consuming()