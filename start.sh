#!/bin/sh

echo "Starting elexon_manager.py..."
python3 elexon_manager.py &

echo "Starting rabbitmq_manager.py..."
python3 rabbitmq_manager.py &

echo "Done"