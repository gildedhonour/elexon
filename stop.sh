#!/bin/sh

echo "Stopping elexon_manager.py..."
pkill -f "python3 elexon_manager.py"

echo "Stopping rabbitmq_manager.py..."
pkill -f "python3 rabbitmq_manager.py"

echo "Done"