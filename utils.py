import os
import os.path
import yaml
import pika
import elexon_dataset_handlers
import time
import pdb
import json
import config
import logging
import smtplib
import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import signal

EMAIL_SUBJECT_OK = "Everything Ok"
EMAIL_SUBJECT_FAILURE = "Something not working"

cfg = config.config
rbt_conn = None

def get_rabbitmq_connection():
  global rbt_conn
  if rbt_conn is None:
    rbt_conn = pika.BlockingConnection(pika.ConnectionParameters(cfg["rabbitmq_server"]["host"]))

  return rbt_conn

subscription_types = cfg["subscription_types"]
subscription_types_objects = {}
for x in subscription_types:
  cls_name = x.capitalize() + "Data"
  obj = vars(elexon_dataset_handlers)[cls_name]()
  subscription_types_objects[x] = cls_name

def loop():
  while True:
    time.sleep(1)

def create_dir(dir_name):
  if not os.path.exists(dir_name):
    os.makedirs(dir_name)

def send_to_rabbitmq(st, msg):
  cls_name = subscription_types_objects[st]
  cls = vars(elexon_dataset_handlers)[cls_name]
  channel = get_rabbitmq_connection().channel()

  # todo - add 5 attempts
  try:
    channel.basic_publish(exchange="", routing_key=st, body=json.dumps(msg), 
      properties=pika.BasicProperties(delivery_mode = 2))
  except pika.exceptions.ConnectionClosed as e:
    rbt_conn = pika.BlockingConnection(pika.ConnectionParameters(cfg["rabbitmq_server"]["host"]))
    channel = rbt_conn.channel()
    cls.rabbitmq_channel = channel
    channel.basic_publish(exchange="", routing_key=st, body=json.dumps(msg),
      properties=pika.BasicProperties(delivery_mode = 2)) #todo - refactor

def create_logger(name, log_file_name):
  logger = logging.getLogger(name)
  fh = logging.FileHandler(log_file_name)
  logger.addHandler(fh)

def send_email(subject, msg_body):
  msg = MIMEMultipart()
  msg["From"] = cfg["email"]["login"]
  msg["To"] = ", ".join(cfg["email"]["recipients"])
  msg["Subject"] = subject
  msg.attach(MIMEText(msg_body))
  server = smtplib.SMTP(cfg["email"]["smtp_server"], cfg["email"]["port"])
  server.ehlo()
  server.starttls()
  server.ehlo()
  server.login(cfg["email"]["login"], cfg["email"]["password"])
  server.sendmail("[{}] {}".format(cfg["email"]["name"], subject), cfg["email"]["recipients"], msg.as_string())
  server.close()
  print("Done")

# todo
def wait_for(func, timeout_sec=5):
  class TimeoutException(Exception):
    pass

  def wait_for_handler(var1, var2):
    raise TimeoutException("End of time")

  signal.signal(signal.SIGALRM, wait_for_handler)
  signal.alarm(timeout_sec)
  res = None
  try:
    func()
    res = True
  except TimeoutException:
    res = False
  finally:
    signal.alarm(0)

  return res